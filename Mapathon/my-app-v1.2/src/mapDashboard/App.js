import React from "react"
import Home from "./Main"
import {BrowserRouter, Switch, Route} from "react-router-dom"
import Editor from "./EditProfile"
import Log from "./Logger"
import leaderboard from "./LeaderBoard"

class MyApp extends React.Component{
render(){
    return(

        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/editProfile" component={Editor}/>
                <Route path="/sign" component={Log}/>
                <Route path="/leaderboard" component={leaderboard}/>
            </Switch>
        </BrowserRouter>

    )
}

}

export default MyApp
