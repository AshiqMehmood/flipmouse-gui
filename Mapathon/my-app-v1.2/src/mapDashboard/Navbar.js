import React, { useState } from "react"
import logo from "/home/ashiq/Documents/my-app/src/assets/mapathon.png"
import {Navbar, Nav} from "react-bootstrap"
import {Link} from "react-router-dom"


function MyNav(props){

    const style={
        color:"#d9d9d9",
        textDecoration: "underline",
        fontFamily:  "Times New Roman"
    }
    const [isLogged, setLogged] = useState(true)

    return(

            <Navbar bg="dark" variant="dark" className="navbar-expand-sm fixed-top nav-tabs ">
               <Link to="/">
                 <Navbar.Brand href="#home" className="navbar-brand active">
                        <img 
                            alt=""
                            src={logo}
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                        />{' '}
                        <span style={style}>Mapthon-Keralam</span>  

                     </Navbar.Brand>        
                </Link>   
                    <Nav className="nav-items">
                    <Link to="/editProfile" className="item1">
                          Edit Profile
                    </Link>
                     <Link to="/sign" className="item2"> 
                         {isLogged ? "Logout" : "Login"}
                     </Link>
                
                      <Link to="/LeaderBoard" className="item3">
                          Status
                      </Link>  
                    </Nav>

            </Navbar>


    )
}

export default MyNav
