import React from "react"
import image from "/home/ashiq/Documents/my-app/src/assets/boy.jpg"
import "./style.css"
import Icon from "./Icon.js"
import {ICONS} from "./constants"
import ScoreBoard from "./ScoreData"

class myReport extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            point: "0",
            
        }
    
    }
    
    render(){
        let customPos1 = {
            position:"absolute",
            top:"45%",
            left:"45%",
            width:"175px",
            height:"180px"
        }
        let customPos2 = {
            position:"absolute",
            top:"45%",
            left:"15%",
            width:"200px",
            height:"200px"
        }
        let customPos3 = {
            position:"absolute",
            top:"45%",
            left:"-10%",
            width:"175px",
            height:"180px"
        }

        const Winners = props => (
                        <div className="col">
                            <div className="custom-card" style={props.customStyle}>
                                 <img src={image} className="card-img-top" alt="edit Photo" />
                                     <h4 className="card-title">{props.customName}</h4>
                                </div>
                            </div>
            )    
        
         const ListGroups = props => (
                <div className="media border p-1">
                <img src={image} alt="Hameroon" className="mr-2 mt-1 rounded-circle" style={{width:"55px"}} />
                 <div className="media-body">
                    <div className="row mt-1 ml-2">
                        <div className="col-10">{props.player}
                            <div className="row-2 mt-1 d-flex">{props.osmID}</div>
                        </div>
                        <div className="col"><span className="badge-success badge-pill">{props.score}</span>
                            <div className="row ml-1">
      
                             {this.handlePrize}   

                            </div>
                        </div>
                    </div>
                 </div>
              </div>
            )


         const PlayerScoreBoard = ScoreBoard.map((item) =>{
            return (
                <ListGroups key={item.id} player={item.name} score={item.score} osmID={item.osm} point={item.score}/>   
            )
            })


            

        return(
            <div className="main-group">
                <div className="sub-group">
                    <div className="row">
                        <Winners customStyle={customPos1} customName="Ahmed Jibran"/>
                        <Winners customStyle={customPos2} customName="Adam Abdullah"/>
                        <Winners customStyle={customPos3} customName="Ashiq Mehmood"/> 
                    </div>
                </div>

                    <div className="container mt-5 w-100 p-2">  
                           {PlayerScoreBoard}
                             
                    </div>
                     
                    
                
            </div> 
        )
    }
}
export default myReport


/**
switch (this.state.point){
    case "1000": return <span><Icon icon={ICONS.CUP}/></span>
    case "10000": return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
    case "100000": return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
}
 <ListGroups score ={this.state.point} />
 if(todo.score.length === 3){
                    return <span><Icon icon={ICONS.CUP}/></span>
                }
                else if(todo.score.length === 4){
                    return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
                }
                else if(todo.score.length === 5){
                    return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
                }
                else if(todo.score.length === 6){
                    return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
                }
**/