import React from "react"
import "./style.css"
import Nav from "./Navbar"
import Profile from "./Profile"
import API from "./API"
import dp from "/home/ashiq/Documents/my-app/src/assets/boy.jpg"
import "./Menu.css"
import Menu from "./Menu"

class Main extends React.Component{
    constructor(){
        super()
        this.state ={
            isLogged:true,
            getUsername:"Ashiq Mehmood",
            getId:"#8812997766",
            getImage:{dp},
            getNumber:"91-8226372836",
            getDate:"15 October 1999",
            getPlace:"Trivandrum",
            getMail:"abcd@gmail.com",
            totalChangeset:"16", 
            changesetBuilding:"8",
            changesetRoad:"9",
            changesetWaterway:"5", 
            changesetPoint:"8", 
            editedBuilding:"5",
            editedRoad:"4", 
            editedWaterway:"3",
            editedPoint:"3",
            visible: false

        }
        this.handleMenuButton = this.handleMouseDown.bind(this)
        this.toggleMenu = this.toggleMenu.bind(this)
       
    }
        async componentDidMount(){
            var userData = await API.get('/',{
                params : {
                    results:1,
                    inc: 'name, email, picture, cell, id, location, dob'
                    
                }
            })
            userData = userData.data.results[0]
            const person = userData.name.first
            const mail = userData.email
            const uniqueID = userData.id.value
            const avatar = userData.picture.large
            const phone = userData.cell
            const country = userData.location.country
            var date = userData.dob.date
                var day = date.substring(8,10)
                var month = date.substring(5,7)
                var year = date.substring(0,4)   
                const newDate = day.concat("-", month,"-",year) 
            this.setState({getUsername:person})
            this.setState({getMail:mail})
            this.setState({getId:uniqueID})
            this.setState({getImage:avatar})
            this.setState({getDate:newDate})
            this.setState({getPlace:country})
            this.setState({getNumber:phone})

        }

        handleMouseDown(e){
            this.toggleMenu()
            console.log("clicked")
            e.stopPropagation()
        }
    
        toggleMenu(){
            this.setState({
                visible: !this.state.visible
            })
        }


    render(){

        const MenuButton = props => (
        
                <button id="roundButton" onMouseDown={props.handleMouseDown}></button>
            
        )
  
        return(
            <div>
              <div className="wrapper">
                 <Nav handleMouseButton={this.toggleMenu}/>
                 <Menu menuVisibility={this.state.visible}/>
             </div>
             <div>
                <Profile 
                    name={this.state.getUsername}
                    id= {"#" + this.state.getId}
                    dp={this.state.getImage}
                    number={this.state.getNumber}
                    date={this.state.getDate}
                    mail={this.state.getMail}
                    place={this.state.getPlace}
                    setTotalChangeset={this.state.totalChangeset}
                    setChangesetBuilding={this.state.changesetBuilding}
                    setChangesetRoad={this.state.changesetRoad}
                    setChangesetWaterway={this.state.changesetWaterway}
                    setChangesetPoint= {this.state.changesetPoint}
                    setEditedBuilding={this.state.editedBuilding}
                    setEditedRoad={this.state.editedRoad} 
                    setEditedWaterway={this.state.editedWaterway}
                    setEditedPoint={this.state.editedPoint}
                    groupVisibility = {this.state.visible}
                />
                
              </div>
             </div>



        )
    }
}
export default Main

