import React from "react"
import {Card, Col, Image,Container, Row,Badge} from "react-bootstrap"
import Icon from "./Icon.js"
import {ICONS} from "./constants"
import Contribution from "./Contribution"
import Building from "/home/ashiq/Documents/my-app/src/assets/build.jpg"
import Road from "/home/ashiq/Documents/my-app/src/assets/road.jpg"
import River from "/home/ashiq/Documents/my-app/src/assets/river.png"
import Park from "/home/ashiq/Documents/my-app/src/assets/park.jpg"
import API from "./API"


class MyProfile extends React.Component{
    constructor(props){
        super(props)
        
    }
/*
  const [totalChangeset, setTotalChangeset] = useState(16)
  const [changesetBuilding, setChangesetBuilding] =useState(9)
  const [changesetRoad, setChangesetRoad] =useState(9)
  const [changesetWaterway, setChangesetWaterway] = useState(8)
  const [changesetPoint, setChangesetPoint] = useState(6)
  const [editedBuilding, setEditedBuilding] = useState(5)
  const [editedRoad, setEditedRoad] = useState(5)
  const [editedWaterway, setEditedWaterway] = useState(5)
  const [editedPoint, setEditedPoint] = useState(5)*/
  render(){
    const {id,name,mail,number,place,date,dp} = this.props
    const {setTotalChangeset,setChangesetBuilding,setChangesetRoad, setChangesetWaterway, setChangesetPoint}=this.props
    const {setEditedRoad,setEditedBuilding,setEditedWaterway,setEditedPoint} =this.props


    var contribGroup={
        viewStyle1:{
            position: "absolute",
            top:'21%',
            left:'15%'
        },
        viewStyle2:{
            position: "absolute",
            top:'21%',
            left:'60%'
        },
        viewStyle3:{
            position: "absolute",
            top:'61%',
            left:'15%'
        },
        viewStyle4:{
            position: "absolute",
            top:'61%',
            left:'60%'
        }

    
    }

        const cardStyle ={
            width: '58rem',
            height:'12rem',
            position: 'relative',
            top:'2%',
            left:'6%',
            borderRadius:'10px'

        }
        const cardStyle2 ={
            width: '58rem',
            height:'28rem',
            position: 'relative',
            top:'3%',
            left:'6%'

        }
        const cardStyle3 ={
            width: '58rem',
            height:'10rem',
            position: 'relative',
            top:'4%',
            left:'6%'

        }

        const idStyle = {
            position: 'relative',
            top:'8%',
            left:'0%',
            width:'auto',
            display: 'inline-block',
            textAlign:'center',
            horizontalAlign:'middle'

            
        }
        var visibility = "stay"

        if(this.props.groupVisibility){
            visibility = "move"
        }

    return(
        <div>
          <div id="main-group" className={visibility}>
              <Card className="bg-light-gray text-black" style={cardStyle}>
                   <Row className="img-border img-dp">
                      <Col xs={10} md={4}>
                            <Image src={dp} roundedCircle/>
                      </Col> 
                   </Row>  
  
                    <Row style={idStyle}>
                      <Col md="1" >
                              <h4 className="card-subtitle mb-2 text-muted font-weight-bold text-justify">{id}</h4> 
                      </Col>
                    </Row>
                <Card.Body className="card-body">
                    <Card.Title><h2 className="font-weight-bold text-left">{name}</h2></Card.Title>

                       <Row>
                       <Col md="auto">
                                <Icon icon={ICONS.TIME} />
                        </Col>
                         <Col md="auto">
                               <Card.Text>Member since {date}</Card.Text>
                        </Col>
                    </Row>
               </Card.Body> 

              <Container className="card2">
                 <Row>
                      <Col md="auto">
                          <Icon className="tag1" icon={ICONS.PHONE} />
                      </Col>
                      <Col md="auto">
                          <h6>{number}</h6>
                      </Col>
                      <Col md="auto">
                          <Icon className="tag2" icon={ICONS.MAIL} />
                      </Col>
                      <Col md="auto">
                          <h6>{mail}</h6>
                       </Col>
                      <Col md="auto">
                          <Icon icon={ICONS.LOCATION} />
                      </Col>
                      <Col md="auto">
                          <h6>{place}</h6>
                      </Col>
                  </Row>
                </Container> <br />
                </Card> 

<Card className="bg-light-gray text-black" style={cardStyle2}>
    <span className="head-text">
        <h5>Contributions</h5>
        <h6>{setTotalChangeset} Changesets</h6>
    </span>
    <Contribution 
        contrib={contribGroup.viewStyle1}
        propName="Buildings"
        propImage={Building}
        addChangeset={setChangesetBuilding}
        editedChangeset={setEditedBuilding}
    />
     <Contribution 
        contrib={contribGroup.viewStyle2}
        propName="Roads"
        propImage={Road}
        addChangeset={setChangesetRoad}
        editedChangeset={setEditedRoad}
        />
                 <Contribution 
                    contrib={contribGroup.viewStyle3}
                    propName="Waterways"
                    propImage={River}
                    addChangeset={setChangesetWaterway}
                    editedChangeset={setEditedWaterway}
                />
                 <Contribution 
                    contrib={contribGroup.viewStyle4}
                    propName="Point of Interest"
                    propImage={Park}
                    addChangeset={setChangesetPoint}
                    editedChangeset={setEditedPoint}
                />
            </Card>  

            <Card className="bg-light text-dark" style={cardStyle3}>
                <h5 className="head-text">Hashtags</h5>
                <Badge className="badger2" pill variant="success"> 
                    #mapathon-keralam
                </Badge>
            </Card>
          </div>
           <div className="footer">
             <h6 className="text-center bg-dark-gray">&#169; Copyright 2020 | Mapathon Keralam</h6>

           </div>  

      </div>


    )
  }
}


export default MyProfile



