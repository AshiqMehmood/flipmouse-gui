                                                                                                     
import React from 'react';
import PropTypes from "prop-types"


const Icon = props => (
  <svg width="18" height="18" viewBox="0 0 1024 1024">
    <path d={props.icon}></path>
  </svg>
);

Icon.propTypes = {
  icon: PropTypes.string,
  //size: PropTypes.number,
  //color: PropTypes.string,
};


export default Icon;



