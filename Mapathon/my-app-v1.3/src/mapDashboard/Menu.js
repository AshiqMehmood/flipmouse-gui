import React from "react"
import "./Menu.css"
import {Link} from "react-router-dom"

class Menu extends React.Component{
    
render(props){
    
    var visibility = "hide"

    if(this.props.menuVisibility){
        visibility = "show"
    }
    
    return(
        <div >
            <nav id="sidebar" className={visibility}>
                <div className="sidebar-header">
                    <h3>Map My Keralam</h3>
                </div>
            <ul className="list-unstyled components">
                <Link to="/">
                    <li className="active">
                        <a href="#">Home</a>
                    </li>
                </Link>
                <Link to="/">
                    <li>
                        <a href="#">About</a>
                    </li>
                </Link>
                <Link to="/LeaderBoard">
                    <li>
                        <a href="#">Leaderboard</a>
                    </li>
                </Link>

                <li>
                    <a href="#">Edit Profile</a>
                </li>
                <li>
                    <a href="#">Sign out</a>
                </li>

            </ul>

            </nav>
            

        </div>
    )
}
}
export default Menu
   


