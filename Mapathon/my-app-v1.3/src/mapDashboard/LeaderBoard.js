import React from "react"
import jsonQ from "jsonq"
import image from "/home/ashiq/Documents/my-app/src/assets/boy.jpg"
import "./style.css"
import Icon from "./Icon.js"
import {ICONS} from "./constants"
import ScoreBoard from "./ScoreData"
import ScoreData from "./JsonScore"


class myReport extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            state_winner: "",
            state_runnerUp: "",
            state_secondRunnerUp: ""
        }
        
    }
    
    render(){
     
        let customPos1 = {
            position:"absolute",
            top:"45%",
            left:"45%",
            width:"175px",
            height:"180px"
        }
        let customPos2 = {
            position:"absolute",
            top:"45%",
            left:"15%",
            width:"200px",
            height:"200px"
        }
        let customPos3 = {
            position:"absolute",
            top:"45%",
            left:"-10%",
            width:"175px",
            height:"180px"
        }

        const Winners = props => (
                        <div className="col">
                            <div className="custom-card" style={props.customStyle}>
                                 <img src={image} className="card-img-top" alt="edit Photo" />
                                     <h4 className="card-title">{props.customName}</h4>
                                </div>
                            </div>
            )    
        
           
        
         const ListGroups = props => (
                <div className="media border p-1">
                <img src={image} alt="Hameroon" className="mr-2 mt-1 rounded-circle" style={{width:"55px"}} />
                 <div className="media-body mt-1" style= {{backgroundColor:"#F0FFF0"}}>
                    <div className="row mt-1 ml-2" >
                        <div className="col-10" >{props.player}
                            <div className="row-2 mt-1 d-flex">{props.osmID}</div>
                        </div>
                        <div className="col"><span className="badge-success badge-pill">{props.score}</span>
                            <div className="row ml-1">
                                {props.point}
                            </div>
                        </div>
                    </div>
                 </div>
              </div>
            )
            
        var sortedArray =  ScoreData.sort(GetSortOrder("score")) 

         const PlayerScoreBoard = sortedArray.map((item) =>{
            return (
                <ListGroups key={item.id} player={item.username} score={item.score} osmID={item.osm}
                 point={<CupList mark={item.score}/>} />   
            )
            })
        function CupList({mark}){
                if(mark > 1000 && mark <5000)
                    return <span><Icon icon={ICONS.CUP} /></span>
                 else if(mark >5000 && mark < 10000)
                    return <span><Icon icon={ICONS.CUP} /><Icon icon={ICONS.CUP} /></span> 
                else if(mark > 10000)     
                    return <span> <Icon icon={ICONS.CUP} /><Icon icon={ICONS.CUP} /><Icon icon={ICONS.CUP} /></span> 
                return(
                    <span> </span>
                )
                    

            }


          function GetSortOrder(prop){
              return function(a,b){
                  if(a[prop] > b[prop]){
                      return -1
                  }
                  else if(a[prop] < b[prop]){
                      return 1
                  }
                  return 0
              }
          }  


            

        return(
           <div className="back-ground">
              <div className="main-group-leaderboard">
                <div className="sub-group">
                    <div className="row">
                        <Winners customStyle={customPos1} customName="Ashiq Mehmood"/>
                        <Winners customStyle={customPos2} customName="Ashiq Mehmood"/>
                        <Winners customStyle={customPos3} customName="Ashiq Mehmood"/> 
                    </div>
                </div>

                    <div className="scroll-box">  
                           {PlayerScoreBoard}         
                             
                    </div>
                                        
                </div>  
            </div> 
        )
    }
}
export default myReport


/**
switch (this.state.point){
    case "1000": return <span><Icon icon={ICONS.CUP}/></span>
    case "10000": return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
    case "100000": return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
}
 <ListGroups score ={this.state.point} />
 if(todo.score.length === 3){
                    return <span><Icon icon={ICONS.CUP}/></span>
                }
                else if(todo.score.length === 4){
                    return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
                }
                else if(todo.score.length === 5){
                    return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
                }
                else if(todo.score.length === 6){
                    return <span><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/><Icon icon={ICONS.CUP}/></span>
                }
                {item.score > 100 ? <span><Icon icon={ICONS.CUP} /></span> : <span>No</span>}
**/