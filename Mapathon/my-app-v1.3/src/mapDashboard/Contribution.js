import React, { useState } from "react"
import {Badge, Col,Row,Image} from "react-bootstrap"

function MyContribution(props){
  
return(
<div style={props.contrib}>
    <div className="row">
        <div className="row">
            <Image src={props.propImage} roundedCircle />

            <div className="col">
                <h5 className="high4">{props.propName}</h5> 

                    <div className="">
                        <div className="col-md-auto">  
                            <p className="badger"> Added
                                <Badge pill variant="success">
                                {props.addChangeset}
                                </Badge></p>
                        </div>
                        <div className="col-md-auto"> 
                            <p className="badger">Edited
                                <Badge pill variant="success">
                                    {props.editedChangeset}
                                </Badge>
                            </p>
                        </div> 
                    </div>  
            </div>          
       </div>
    </div>
 </div>    
    )
}

export default MyContribution