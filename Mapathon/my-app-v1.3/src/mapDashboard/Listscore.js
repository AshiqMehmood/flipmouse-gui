import React from "react"
import ScoreData from "./JsonScore"
import image from "/home/ashiq/Documents/my-app/src/assets/boy.jpg"
import jsonQ from "jsonq"
import scoreData from "./ScoreData"

class ListGroup extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            state_winner: false,
            state_runnerUp: false,
            state_secondRunnerUp: false
        }

    }
 
    render(){
        var cupChooser = () => {
            let data = jsonQ(scoreData)
            let score = {}
            let player = {}
            let userName = {}
            let userScore = {}
            score =  data.find('score').value()
            score.sort(function(a,b){return b-a})
            player = data.find('score',function(){
                return this > 0
            })
            player.each(function(index, path, value){
                userName = value.username 
                userScore = value.score
                console.log(userName + ' ' + userScore)
                return false
            })
            
        }
        
       
        return(
            <div className="media border p-1">
            <img src={image} alt="Hameroon" className="mr-2 mt-1 rounded-circle" style={{width:"55px"}} />
             <div className="media-body mt-1" style= {{backgroundColor:"#F0FFF0"}}>
                <div className="row mt-1 ml-2" >
                    <div className="col-10" >{props.player}
                        <div className="row-2 mt-1 d-flex">{props.osmID}</div>
                    </div>
                    <div className="col"><span className="badge-success badge-pill">{props.score}</span>
                        <div className="row ml-1">
                            {cupChooser()}
                        </div>
                    </div>
                </div>
             </div>
          </div>
        )
    }
}
export default ListGroup