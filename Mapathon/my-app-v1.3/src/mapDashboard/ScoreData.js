const scoreData = [
    {
        "id":"1",
        "name": "Agmor Tint",
        "score": 37,
        "osm": "52663"
    },
    {
        "id":"2",
        "name": "Adam James Potter",
        "score": 47888,
        "osm": "43521"
    },
    {
        id:3,
        name: "Ashiq Mehmood",
        score: 899912,
        osm: 66352
    },
    {
        id:4,
        name: "Simpy Haber B",
        score: 6661,
        osm:82771
    },
    {
        id:5,
        name: "Mobret Kooper",
        score: 201,
        osm: 42271
    }
]

export default scoreData