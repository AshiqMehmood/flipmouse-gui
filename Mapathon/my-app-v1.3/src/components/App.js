import React from "react"
import Login from "./Login"
import Bg from "./Background"
import Logo from "./logo"

function MyApp(){
    return(
        <div>
            <Bg />
            <Login />
            <Logo />
            <h3 className="head-text">Mapathon-kerala</h3>
        </div>
    )
}
export default MyApp