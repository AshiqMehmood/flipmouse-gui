#ifndef _BUTTONS_H_
#define _BUTTONS_H_

#define NUMBER_OF_BUTTONS  19 //physical +virtual switches
#define NUMBER_OF_PHYSICAL_BUTTONS  3
#define NUMBER_OF_LEDS      1

#define DEFAULT_DEBOUNCING_TIME  5

#define UP_BUTTON         3
#define DOWN_BUTTON       4     
#define LEFT_BUTTON       5       
#define RIGHT_BUTTON      6       

#define SIP_BUTTON               7
#define STRONGSIP_BUTTON         8
#define PUFF_BUTTON              9
#define STRONGPUFF_BUTTON       10


#define STRONGSIP_UP_BUTTON     11
#define STRONGSIP_DOWN_BUTTON   12
#define STRONGSIP_LEFT_BUTTON   13
#define STRONGSIP_RIGHT_BUTTON  14

#define STRONGPUFF_UP_BUTTON    15
#define STRONGPUFF_DOWN_BUTTON  16
#define STRONGPUFF_LEFT_BUTTON  17
#define STRONGPUFF_RIGHT_BUTTON 18  

struct slotButtonSettings {
  uint16_t mode;
  uint16_t value;
};

struct buttonDebouncerType {
  uint8_t bounceCount;
  uint8_t bounceState;
  uint8_t stableState;
  uint8_t longPressed;
  uint32_t timestamp;
};
extern struct slotButtonSettings buttons[NUMBER_OF_BUTTONS];
extern char* keystringButtons[NUMBER_OF_BUTTONS];
extern uint16_t keystringBufferLen;

//func declarations
//uint16_t storeKeystringButton(uint8_t buttonIndex, char * text);
//uint16_t deleteKeystringButton(uint8_t buttonIndex);
void handlePress(int buttonIndex);
void handleRelease(int buttonIndex);
uint8_t handleButton(int i, uint8_t state);
uint8_t inHoldMode(int i);
void initDebouncers();


#endif

