#ifndef _MOUSER_H_
#define _MOUSER_H_

#include <Arduino.h> 
#include <string.h>
#include <stdint.h>
#include <avr/pgmspace.h>
#include "commands.h"
#include "buttons.h"
#include "hid_hal.h"
#include "modes.h"

#define VERSION_STRING "Flipmouse v2.81"
#define ARDUINO_PRO_MICRO

#ifdef ARDUINO_PRO_MICRO
 #include <Mouse.h>
 #include <Keyboard.h>
 #define EEPROM_SIZE 1023
#endif

#define MAX_NAME_LEN                 10 //slot name
#define MAX_SLOTS                     7
#define MAX_KEYSTRINGBUFFER_LEN      500
#define MAX_CMDLEN                   200 //max length of a single AT command
#define WORKINGMEM_SIZE              300
#define MAX_KEYSTRING_LEN   (WORKINGMEM_SIZE-3)

#define PARTYPE_NONE  0
#define PARTYPE_UINT  1
#define PARTYPE_INT   2
#define PARTYPE_STRING  3

#define REPORT_NONE  0  
#define REPORT_ONE_SLOT  1
#define REPORT_ALL_SLOTS 2

#define TONE_CALIB            1
#define TONE_CHANGESLOT       2
#define TONE_ENTER_STRONGSIP  3
#define TONE_EXIT_STRONGSIP   4
#define TONE_ENTER_STRONGPUFF 5
#define TONE_EXIT_STRONGPUFF  6
#define TONE_INDICATE_SIP     7
#define TONE_INDICATE_PUFF    8

#define CLICK_TIME    8
#define DEBUG_FULLOUTPUT      1

#define BUTTON1_PRESS_TIME_FOR_PAIRING 800
#define DEFAULT_CLICK_TIME      8

#define DEFAULT_WHEEL_STEPSIZE  3

extern uint8_t workingmem[WORKINGMEM_SIZE];

struct slotGeneralSettings {
  char slotName[MAX_NAME_LEN];
  uint8_t stickMode; // 0-alternatice, 1-mouse
  uint8_t ax;        //acceleration x
  uint8_t ay;        //acceleration y
  int16_t dx;        //deadzone x
  int16_t dy;        //deadzone y
  uint16_t ms;       //maximum speed
  uint16_t ac;       //acceleration time
  uint16_t ts;      //threshold sip         
  uint16_t tp;      //threshold puff
  uint8_t ws;       //wheel stepsize
  uint16_t sp;      //threshold strong puff 
  uint16_t ss;      //threshold strong sip
  uint8_t gu;         //gain loop
  uint8_t gd;         //gain down
  uint8_t gl;         //gain left
  uint8_t gr;         //gain right
  int16_t cx;         //calib x
  int16_t cy;         //calib y
  uint16_t ro;        //orientation(0.90,180,270)
  uint8_t bt;         //bt-mode (0,1,2) //char  ii[MAX_NAME_LEN];
 
};

struct atCommandType {              // holds settings for a button function 
  char atCmd[3];
  uint8_t  partype;   // type of parameter: int, uint or string
};

extern uint8_t DebugOutput;
extern uint8_t actSlot;
extern uint8_t reportSlotParameters;
extern uint8_t reportRawValues;
extern struct slotGeneralSettings settings; //can be accessed to all files
//extern char slotName[MAX_NAME_LEN];
extern int EmptySlotAddress;

extern const struct atCommandType atCommands[];
extern int8_t  input_map[NUMBER_OF_PHYSICAL_BUTTONS];
extern int8_t modeSelector; 

extern char keystringBuffer[MAX_KEYSTRINGBUFFER_LEN];     //buffer for all string parametrs for the buttons
extern char cmdstring[MAX_CMDLEN];                        //buffer for incoming AT commands
extern const int usToDE[];

extern uint16_t calib_now;
extern int16_t  cx;
extern int16_t  cy;
extern int8_t moveX;       
extern int8_t moveY;
extern double force;
extern double angle;


void performCommand(uint8_t cmd, int16_t par1, char * keystring, int8_t periodicMouseMovement);
void saveToEEPROM(char * slotname);
void readFromEEPROM(char * slotname);
void deleteSlots();
void listSlots();
void initButtons();
void printCurrentSlot();
void release_all();

//keyboard funcs from FABI(Mouser.ino)
uint16_t  keystringMemUsage(uint8_t button);
char * getKeystring (uint8_t button);
void setKeystring (uint8_t button, char * text);
void printKeystrings ();
//keys.cpp
int getKeycode(char*);
void sendToKeyboard(char *);
void pressSingleKeys(char* text);
void releaseSingleKeys(char* text);

int freeRam();
void parseByte(int newByte);
void parseCommand(char * cmdstr);


#define strcpy_FM strcpy_PF
#define strcmp_FM strcmp_PF
typedef uint_farptr_t uint_farptr_t_FM;


#endif
  


   
  


