package jsscCode;

import java.io.*; // IOException
import java.util.*; // Scanner
import java.util.concurrent.ScheduledExecutorService;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.lang.String;
import jssc.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
	static JFrame frame =new JFrame();
	static JComboBox<String> cb =new JComboBox<String>();
	static JButton connect = new JButton("Connect");
	static JSlider slider = new JSlider();
	static JTextArea console1 =new JTextArea(10,10);
	static JTextArea console =new JTextArea(7,30);
	static JScrollPane scroller = new JScrollPane (console, 
			   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	static JScrollPane scroller1 = new JScrollPane (console1, 
			   JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
    static JPanel panel = new JPanel();
    static JPanel panel1 = new JPanel();
	static JButton on = new JButton("LED ON");
	static JButton off =new JButton("LED OFF");
	static String portName;
	static String[] portNames;
	static String selected;
	
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    	    	 slider.setMaximum(1023);
    	console.setEditable(false);
    	frame.setTitle("Flipmouse GUI");
    	frame.setSize(600,600);	
    	frame.setLocationRelativeTo(null);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	  
    	portNames = SerialPortList.getPortNames();
     	    for (String string : portNames) {
     	        cb.addItem(string);
        }
      /**  cb.addActionListener(new ActionListener(){
  	   @Override public void actionPerformed(ActionEvent e) {
  		 selected = cb.getSelectedItem().toString();
  	   
  	     }
       });**/
    
    	// portNames = SerialPortList.getPortNames();
        
        if (portNames.length == 0) {
            System.out.println("There are no serial-ports :( You can use an emulator, such ad VSPE, to create a virtual serial port.");
            System.out.println("Press Enter to exit...");
            try {
                System.in.read();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return;
        }
        
    
    /**  System.out.println("Available com-ports:");
        for (int i = 0; i < portNames.length; i++){
            System.out.println(portNames[i]);
        }
        System.out.println("Type port name, which you want to use, and press Enter...");
        Scanner in = new Scanner(System.in);
         portName = in.next();
    	**/
        // writing to port
        SerialPort serialPort = new SerialPort(cb.getSelectedItem().toString()); //comboBox action listener does not pass string value correctly
        connect.addActionListener(new ActionListener() {
        	@Override public void actionPerformed(ActionEvent e) {
        		 try {
        	            // opening port
        	            serialPort.openPort();
        	            
        	            serialPort.setParams(SerialPort.BAUDRATE_9600,
        	                                 SerialPort.DATABITS_8,
        	                                 SerialPort.STOPBITS_1,
        	                                 SerialPort.PARITY_NONE);
        	            
        	            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | 
        	                                          SerialPort.FLOWCONTROL_RTSCTS_OUT);
        	            
        	             serialPort.addEventListener(new PortReader(), SerialPort.MASK_RXCHAR);
        	             
        	            System.out.println("Device connected Successfully !!");
        	           
        	        }
        	        catch (SerialPortException ex) {
        	            System.out.println("Error in writing data to port: " + ex);
        	        }
        	}
        });
        
       on.addActionListener(new ActionListener() {
        	@Override public void actionPerformed(ActionEvent e) {
        		try {
        		serialPort.writeString("*#*sp");
            		console1.setText(serialPort.readString());
            		redirectSystemStreams(); 	
 	            	
      	            }
   		
            	catch(SerialPortException ex) {
            		System.out.println("Error in writing data to port: " + ex);
            	}
        	}
        });
        off.addActionListener(new ActionListener() {
        	@Override public void actionPerformed(ActionEvent e) {
        		try {
        		  	
            		serialPort.writeString("exit");
            		 console1.setText(serialPort.readString());
            		 redirectSystemStreams(); 	
            		
            	}catch(SerialPortException ex) {
            		System.out.println("Error in writing data to port: " + ex);
            	}
        	}
        });
       
     
      /**  on.addActionListener(new ActionListener() {
        	@Override public void actionPerformed(ActionEvent e) {
        		
        	}
        });**/
       // final ScheduledExecutorService ses = Executors.newScheduledThreadPool(2);
        //ses.scheduleAtFixedRate(readSensorValue(),0,1,TimeUnit.SECONDS);
       	      
        
     panel.add(connect);
   	 panel.add(cb);
   	 panel.add(on);
   	 panel.add(off);
   	 //panel.add(console);
   	 panel.add(scroller);
   	 panel.add(scroller1);
   	 panel.add(slider);
   	 frame.add(panel);
   	 
   	 frame.setVisible(true);
   
  }
    
    	
   	 
     
    
    // receiving response from port
    private static class PortReader implements SerialPortEventListener {
    	 SerialPort serialPort = new SerialPort(cb.getSelectedItem().toString());
        @Override
        public void serialEvent(SerialPortEvent event) {
            if(event.isRXCHAR() && event.getEventValue() > 0) {
                try {
                   
                    String receivedData = serialPort.readString(event.getEventValue()); 
                    System.out.println("Received response from port: " + receivedData);
                    
                }
                catch (SerialPortException ex) {
                    System.out.println("Error in receiving response from port: " + ex);
                }
            }
        }
    }
    
 //function to redirect console values to textarea   
    public static  void updateTextArea(final String text) {
	  SwingUtilities.invokeLater(new Runnable() {
	    public void run() {
		console.append(text);
	    }
	  });
	}
	 
public static void redirectSystemStreams() {
	  OutputStream out = new OutputStream() {
	    @Override
	    public void write(int b) throws IOException {
	      updateTextArea(String.valueOf((char) b));
	    }
	 
	    @Override
	    public void write(byte[] b, int off, int len) throws IOException {
	      updateTextArea(new String(b, off, len));
	    }
	 
	    @Override
	    public void write(byte[] b) throws IOException {
	      write(b, 0, b.length);
	    }
	  };			
	  System.setOut(new PrintStream(out, true));
	  System.setErr(new PrintStream(out, true));
	}

public static Runnable readSensorValue() {
	try {
		SerialPort serialPort = new SerialPort(cb.getSelectedItem().toString()); 
		 console1.append(serialPort.readString());
	}
    catch(Exception ex) {}
	return null;
	  
 }
		
}