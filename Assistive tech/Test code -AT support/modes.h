#ifndef _MODES_H_
#define _MODES_H_

#define HOLD_IDLE 0
#define HOLD X    1
#define HOLD_Y    2
#define HOLD_PRESSURE 3

#define STICKMODE_ALTERNATIVE   0
#define STICKMODE_MOUSE         1

void handleModeState(int x, int y, int pressure);

#endif

