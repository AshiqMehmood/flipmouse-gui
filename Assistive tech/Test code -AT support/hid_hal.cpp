#include "hid_hal.h"

void mouseRelease(uint8_t button){
  if(settings.bt & 1)        // bitwise AND operation to check if bluetooth module is connected or not
  Mouse.release(button);     // (settings.bt)0000 & (1)0001  = 0000
}

void mousePress(uint8_t button){
  if(settings.bt & 1)
  Mouse.press(button);
}

/**void mouseScroll(int8_t steps){
  if(settings.bt & 1)
  Mouse.scroll(steps);
}**/

void mouseMove(int x, int y){
  while(x< -128){
    if(settings.bt & 1)
       Mouse.move(-128,0);
    x+=128; 
  }

  while(x>127){
    if(settings.bt & 1)
       Mouse.move(127,0);
    x-=127;
  }
  while(y<-128){
    if(settings.bt & 1)
       Mouse.move(0, -128);
    y+=128;  
  }

  while(y>127){
    if(settings.bt & 1)
        Mouse.move(0,127);
    y-=127;    
  }
  if (settings.bt & 1) 
      Mouse.move(x, y);
}
/**void keyboardPress(int key)
{
  if (settings.bt & 1)
        Keyboard.press(key);
}

void keyboardRelease(int key)
{
  if (settings.bt & 1)
      Keyboard.release(key);   
}

void keyboardReleaseAll()
{
  if (settings.bt & 1)
     Keyboard.releaseAll();
}
**/


